## Cara Pemasangan

Pasang beberapa peralatan berikut terlebih dahulu :

1. Maven
2. Docker

Prosedur pemasangan :

1. download kode-nya dari repository ini
2. ubah `url` di `src/main/resources/application.yml`, sesuaikan dengan alamat server sismiop, bila perlu ubah port-nya juga 
3. dengan maven, lakukan kompilasi dengan perintah berikut :
```$xslt
mvn clean package
```
4. Seharusnya terbentuk folder `target`. Lakukan pembentukan image docker dan jalankan dengan perintah berikut ;
```$xslt
$ docker build -t switch-bpn
$ docker run -d -p 5001:5001 switch-bpn
```
5. Lakukan cek akses ke server dengan _browser_

### API

* `http://localhost:5001/getAllData` melakukan pengambilan seluruh data objek dan subjek pajak di tahun berjalan
* `http://localhost:5001/getData/{nop}` melakukan pengambilan data objek dan subjek pajak berdasarkan NOP di tahun berjalan


##### note:

Bila dirasa aga ribet, infokan aja ke saya ya, alternatif yang bisa dilakukan adalah :

1. cukup install docker di salah satu server atau komputer, yang tentunya ada koneksi langsung (LAN) dengan server oracle SISMIOP
2. infokan alamat IP, nama DB sismiop, dan nama Kab/Kota.
3. jalankan perintah docker berikut :

```$xslt
$ docker run -d -p {port}:5001 tamami/switch-bpn-{kab/kota}
```

Keterangan :

* `{port}` diisi dengan alamat _port_ yang tersedia, barangkali `5001` sudah terpakai oleh layanan lain
* `{kab/kota}` nantinya digantikan dengan format nama Kabupaten/Kota seperti contohnya : `switch-bpn-brebeskab` untuk Kab. Brebes