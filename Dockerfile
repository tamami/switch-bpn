FROM openjdk:8-jdk-alpine

WORKDIR /app
COPY target/*.jar /app/app.jar

RUN addgroup dev && adduser dev -G dev -D
USER dev

EXPOSE 5001

ENTRYPOINT [ "java", "-jar", "./app.jar" ]