package lab.aikibo.switchbpn.services

import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class SpptServicesTest {

    @Autowired
    lateinit var spptServices: SpptServices

    @Test
    fun getByNopTest() {
        val nop = "332901000100100010"
        val result = spptServices.getByNop(nop)
        result.doOnSuccess {
            println(it.namaWp)
            Assert.assertNotNull(it)
        }

    }

}