package lab.aikibo.switchbpn.repo

import lab.aikibo.switchbpn.entity.id.SpptPK
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class SpptRepoTest {

    @Autowired
    lateinit var spptRepo: SpptRepo

    @Test
    fun findByIdTest() {
        val nop = "332901000100100010"
        val result = spptRepo.findById(SpptPK(
                nop.substring(0,2), nop.substring(2,4), nop.substring(4,7), nop.substring(7,10),
                nop.substring(10,13), nop.substring(13,17), nop.substring(17,18), "2019"
        ))
        println(result.get().nmWpSppt)
        Assert.assertNotNull(result)
    }

    /*
    @Test
    fun findAll() {
        val result = spptRepo.findAll()
        println(result.size)
        Assert.assertNotNull(result)
    }
     */

}