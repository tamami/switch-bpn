package lab.aikibo.switchbpn.services

import lab.aikibo.switchbpn.entity.Sppt
import lab.aikibo.switchbpn.entity.id.SpptPK
import lab.aikibo.switchbpn.model.ResponseData
import lab.aikibo.switchbpn.repo.SpptRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*
import javax.xml.ws.Response

@Service
class SpptServices {

    @Autowired
    lateinit var spptRepo: SpptRepo

    fun getByNop(nop: String): Mono<ResponseData> {
        val thisYear = Calendar.getInstance().get(Calendar.YEAR)
        var tempResult = spptRepo.findById(
                SpptPK(
                    nop.substring(0,2), nop.substring(2,4), nop.substring(4,7),
                    nop.substring(7,10), nop.substring(10,13), nop.substring(13,17),
                        nop.substring(17,18), thisYear.toString()
                )
        )
        var responseData = ResponseData(nop, tempResult.get().njopBumiSppt, tempResult.get().luasBumiSppt,
                tempResult.get().nmWpSppt, tempResult.get().tglJatuhTempoSppt, "")
        return Mono.just(responseData)
    }

    fun getAll(): Flux<ResponseData> {
        val thisYear = Calendar.getInstance().get(Calendar.YEAR)
        var tempResult = spptRepo.findByThnPajakSppt(thisYear.toString())
        var result = mutableListOf<ResponseData>()

        for(sppt in tempResult) {
            result.add(ResponseData(
                    sppt.kdPropinsi + sppt.kdDati2 + sppt.kdKecamatan + sppt.kdKelurahan + sppt.kdBlok + sppt.noUrut + sppt.kdJnsOp,
                    sppt.njopBumiSppt, sppt.luasBumiSppt, sppt.nmWpSppt, sppt.tglJatuhTempoSppt, "")
            )
        }
        return Flux.fromIterable(result)
    }

}