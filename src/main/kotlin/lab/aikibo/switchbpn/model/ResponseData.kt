package lab.aikibo.switchbpn.model

import java.math.BigDecimal
import java.util.*

data class ResponseData(
        var nop: String,
        var njopBumi: BigDecimal,
        var luasBumi: BigDecimal,
        var namaWp: String,
        var tglJatuhTempo: Date,
        var kodePelunasan: String
)