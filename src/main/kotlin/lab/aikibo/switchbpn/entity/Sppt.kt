package lab.aikibo.switchbpn.entity

import lab.aikibo.switchbpn.entity.id.SpptPK
import java.math.BigDecimal
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "sppt")
@IdClass(SpptPK::class)
data class Sppt(
    @Id @Column(name = "kd_propinsi") // 1
    var kdPropinsi: String,
    @Id @Column(name = "kd_dati2") // 2
    var kdDati2: String,
    @Id @Column(name = "kd_kecamatan") // 3
    var kdKecamatan: String,
    @Id @Column(name = "kd_kelurahan") // 4
    var kdKelurahan: String,
    @Id @Column(name = "kd_blok") // 5
    var kdBlok: String,
    @Id @Column(name = "no_urut") // 6
    var noUrut: String,
    @Id @Column(name = "kd_jns_op") // 7
    var kdJnsOp: String,
    @Id @Column(name = "thn_pajak_sppt") // 8
    var thnPajakSppt: String,
    @Column(name = "siklus_sppt") // 9
    var siklusSppt: Int,
    @Column(name = "kd_kanwil_bank") // 10
    var kdKanwilBank: String,
    @Column(name = "kd_kppbb_bank") // 11
    var kdKppbbBank: String,
    @Column(name = "kd_bank_tunggal") // 12
    var kdBankTunggal: String,
    @Column(name = "kd_bank_persepsi") // 13
    var kdBankPersepsi: String,
    @Column(name = "kd_tp") // 14
    var kdTp: String,
    @Column(name = "nm_wp_sppt") // 15
    var nmWpSppt: String,
    @Column(name = "jln_wp_sppt") // 16
    var jlnWpSppt: String,
    @Column(name = "blok_kav_no_wp_sppt") // 17
    var blokKavNoWpSppt: String,
    @Column(name = "rw_wp_sppt") // 18
    var rwWpSppt: String,
    @Column(name = "rt_wp_sppt") // 19
    var rtWpSppt: String,
    @Column(name = "kelurahan_wp_sppt") // 20
    var kelurahanWpSppt: String,
    @Column(name = "kota_wp_sppt") // 21
    var kotaWpSppt: String,
    @Column(name = "kd_pos_wp_sppt") // 22
    var kdPosWpSppt: String,
    @Column(name = "npwp_sppt") // 23
    var npwpSppt: String,
    @Column(name = "no_persil_sppt") // 24
    var noPersilSppt: String,
    @Column(name = "kd_kls_tanah") // 25
    var kdKlsTanah: String,
    @Column(name = "thn_awal_kls_tanah") // 26
    var thnAwalKlsTanah: String,
    @Column(name = "kd_kls_bng") // 27
    var kdKlsBng: String,
    @Column(name = "thn_awal_kls_bng") // 28
    var thnAwalKlsBng: String,
    @Column(name = "tgl_jatuh_tempo_sppt") // 29
    var tglJatuhTempoSppt: Date,
    @Column(name = "luas_bumi_sppt") // 30
    var luasBumiSppt: BigDecimal,
    @Column(name = "luas_bng_sppt") // 31
    var luasBngSppt: BigDecimal,
    @Column(name = "njop_bumi_sppt") // 32
    var njopBumiSppt: BigDecimal,
    @Column(name = "njop_bng_sppt") // 33
    var njopBngSppt: BigDecimal,
    @Column(name = "njop_sppt") // 34
    var njopSppt: BigDecimal,
    @Column(name = "njoptkp_sppt") // 35
    var njoptkpSppt: BigDecimal,
    @Column(name = "njkp_sppt") // 36
    var njkpSppt: BigDecimal,
    @Column(name = "pbb_terhutang_sppt") // 37
    var pbbTerhutangSppt: BigDecimal,
    @Column(name = "faktor_pengurang_sppt") // 38
    var faktorPengurangSppt: BigDecimal,
    @Column(name = "pbb_yg_harus_dibayar_sppt") // 39
    var pbbYgHarusDibayarSppt: BigDecimal,
    @Column(name = "status_pembayaran_sppt") // 40
    var statusPembayaranSppt: Char,
    @Column(name = "status_tagihan_sppt") // 41
    var statusTagihanSppt: Char,
    @Column(name = "status_cetak_sppt")  // 42
    var statusCetakSppt: Char,
    @Column(name = "tgl_terbit_sppt")   // 43
    var tglTerbitSppt: Date,
    @Column(name = "tgl_cetak_sppt")    // 44
    var tglCetakSppt: Date,
    @Column(name = "nip_pencetak_sppt") // 45
    var nipPencetakSppt: String
)